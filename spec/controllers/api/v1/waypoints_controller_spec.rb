require 'rails_helper'

module Api::V1
  RSpec.describe WaypointsController, type: :controller do
    describe "POST #create" do

      let(:subject){ WaypointsController.new }
      let(:do_create) do
        post :create,
          params: {
            latitude: -33.406140,
            longitude: -70.561390,
            sent_at: '2017-07-04 12:53:43',
            vehicle_identifier: 'HA-3452'
          },
          format: :json
      end

      context 'on sending waypoints' do

        it 'success' do
          subject.class.skip_before_action :enqueue_waypoint
          do_create
          expect(response).to have_http_status(:success)
        end

      end
    end
  end
end
