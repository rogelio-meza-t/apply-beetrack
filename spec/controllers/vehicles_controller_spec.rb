require 'rails_helper'

RSpec.describe VehiclesController, type: :controller do
  describe "GET #show" do
    context 'on load page' do
      it 'responds with  success' do
        get :show
        expect(response).to be_success
      end

      it 'show a list of vehicles' do
        v1 = Vehicle.create(identifier: "V1")
        v2 = Vehicle.create(identifier: "V2")
        v3 = Vehicle.create(identifier: "V3")
        get :show
        expect(assigns(:vehicles)).to match_array([v1,v2,v3])
      end

      it 'renders show template' do
        get :show
        expect(response).to render_template(:show)
      end
    end
  end
end
