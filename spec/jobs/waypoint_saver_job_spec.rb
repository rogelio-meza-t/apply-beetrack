require 'rails_helper'

RSpec.describe WaypointSaverJob, type: :job do
  include ActiveJob::TestHelper

  subject(:job) {described_class.perform_later(gps_params)}

  let(:gps_params) do
    {
      latitude: -33.406140,
      longitude: -70.561390,
      sent_at: "2017-07-04 17:53:22",
      vehicle_identifier: "HA-3452"
    }
  end

  context 'given valid gps params' do
    it 'queues the job' do
      expect{ job }.to have_enqueued_job(described_class)
    end

    it 'save the waypoint' do
      expect(WaypointSaverJob).to receive(:perform_later).with(gps_params)
      perform_enqueued_jobs { job }
    end
  end

  after do
    clear_enqueued_jobs
    clear_performed_jobs
  end
end
