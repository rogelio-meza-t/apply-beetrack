class RemoveAttributeFromWaypoint < ActiveRecord::Migration[5.1]
  def change
    remove_column :waypoints, :vehicle_identifier
  end
end
