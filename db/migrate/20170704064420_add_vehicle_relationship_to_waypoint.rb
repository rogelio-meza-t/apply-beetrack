class AddVehicleRelationshipToWaypoint < ActiveRecord::Migration[5.1]
  def change
    add_reference :waypoints, :vehicle, index: true
  end
end
