class CreateWaypoint < ActiveRecord::Migration[5.1]
  def change
    create_table :waypoints do |t|
      t.decimal :latitude, precision: 9, scale: 6
      t.decimal :longitude, precision: 9, scale: 6
      t.datetime :sent_at
      t.string :vehicle_identifier
    end
  end
end
