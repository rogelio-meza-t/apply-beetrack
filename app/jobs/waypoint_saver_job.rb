class WaypointSaverJob < ApplicationJob

  def perform(gps_params)
    @gps_params = gps_params
    @vehicle = Vehicle.find_or_create_by(identifier: vehicle_identifier)

    Waypoint.create(waypoint_params)
  end

  # returns the waypoint data and creates the vehicle if necessary
  def waypoint_params
    {
      latitude: @gps_params[:latitude],
      longitude: @gps_params[:longitude],
      sent_at: @gps_params[:sent_at],
      vehicle_id: @vehicle.id
    }
  end

  def vehicle_identifier
    @gps_params[:vehicle_identifier]
  end
end
