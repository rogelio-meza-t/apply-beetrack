$(document).on('turbolinks:load', function(){
    var mymap = L.map('map').setView([51.505, -0.09], 13);
    L.tileLayer(
        'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}',
        {
            foo: 'bar'
        }
    ).addTo(mymap);

    // add markers to the map
    for(var key in gon.vehicles){
        var lat = gon.vehicles[key].last_waypoint.latitude;
        var lng = gon.vehicles[key].last_waypoint.longitude;
        var vehicle = gon.vehicles[key].identifier;
        var sent_at = gon.vehicles[key].last_waypoint.sent_at;

        var date = new Date(sent_at);

        var marker = L.marker([lat, lng])
            .addTo(mymap)
            .bindPopup("Vehicle "+ vehicle)
            .openPopup();

        // add vehicle to the side list
        container = '<div class="vehicle-container" data-lat="'+lat+'" data-lng="'+lng+'">'
        container += '<div class="title"> Vehicle: '+vehicle+'</div>'
        container += '<div class="coords">Last position'
        container += ' ['+lat+', '+lng+']'
        container += '</div>'
        container += '<div class="tie">at '+date.toUTCString()+'</div>'
        container += '</div>'
        $('#vehicle-list').append(container);
    }

    $('#vehicle-list').on('click', '.vehicle-container', function(){
        var lat = $(this).data('lat');
        var lng = $(this).data('lng');
        mymap.panTo(new L.LatLng(lat,lng));
    })
});
