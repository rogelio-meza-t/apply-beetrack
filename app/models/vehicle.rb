# == Schema Information
#
# Table name: vehicles
#
#  id         :integer          not null, primary key
#  identifier :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Vehicle < ApplicationRecord
  validates :identifier, presence: true, uniqueness: true

  has_many :waypoints

  # returns the recent waypoint
  def last_waypoint
    self.waypoints.first
  end
end
