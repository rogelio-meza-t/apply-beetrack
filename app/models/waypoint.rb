# == Schema Information
#
# Table name: waypoints
#
#  id         :integer          not null, primary key
#  latitude   :decimal(9, 6)
#  longitude  :decimal(9, 6)
#  sent_at    :datetime
#  vehicle_id :integer
#

class Waypoint < ApplicationRecord
  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :sent_at, presence: true

  belongs_to :vehicle

  # sort by default the waypoints from recent to older
  default_scope { order(sent_at: :desc)  }

end
