class VehiclesController < ApplicationController
  def show
    # get all vehicles
    @vehicles = Vehicle.all

    # filter the waypoints and return the recent
    filtered = @vehicles.includes(:waypoints).as_json(include: :last_waypoint)

    # send the vehicles to the view as JSON
    gon.vehicles = filtered
  end
end
