module Response
  def json_response(status = :ok)
    render json: {message: json_response_message(status)},
      status: status
  end

  def json_response_message(status)
    case status
    when :ok
      "Object stored successfuly"
    when :internal_server_error
      "Object not saved"
    else
      "Undefined error"
    end
  end
end
