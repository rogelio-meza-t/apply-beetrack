module Api
  module V1
    class WaypointsController < ApplicationController
      protect_from_forgery except: :create

      before_action :enqueue_waypoint, only: [:create]

      # POST /api/v1/gps
      def create
        respond_to do |format|
          format.json do
            render json: {message: "The waypoint is beign processed"}
          end
        end
      end

      private

      # send the storage process to the background
      def enqueue_waypoint
        WaypointSaverJob.perform_later(gps_params)
      end

      def waypoint_params
        params.permit(:latitude, :longitude, :sent_at)
      end

      def gps_params
        params.permit(:latitude, :longitude, :sent_at, :vehicle_identifier).to_h
      end
    end
  end
end
