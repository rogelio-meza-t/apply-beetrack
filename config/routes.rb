Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :gps, controller: "waypoints", only: [:create]
    end
  end

  get :show, to: "vehicles#show", as: :show_vehicles
end
